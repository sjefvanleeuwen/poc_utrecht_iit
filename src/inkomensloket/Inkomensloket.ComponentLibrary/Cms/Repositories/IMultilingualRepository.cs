﻿using System.Globalization;

namespace Inkomensloket.ComponentLibrary.Cms.Repositories
{
    public interface IMultilingualRepository<TEntity> 
        where TEntity : IMultilingualEntity
    {
        TEntity Get(string key, CultureInfo culture);
    }
}
