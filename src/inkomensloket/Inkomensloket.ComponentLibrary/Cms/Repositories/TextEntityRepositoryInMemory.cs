﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Inkomensloket.ComponentLibrary.Cms.Repositories
{
    public class TextEntityRepositoryInMemory : IMultilingualRepository<TextEntity>
    {
        public TextEntity Get(string key, CultureInfo culture)
        {
            return EntityData.Single(p => p.Id == key && p.Culture == culture);
        }

        private static IEnumerable<TextEntity> EntityData = new List<TextEntity>()
        {
            new TextEntity(){Culture = CultureInfo.GetCultureInfo("nl-NL"), Text="Gegevens ophalen bij de Belastingdienst",Id="1" },
            new TextEntity(){Culture = CultureInfo.GetCultureInfo("nl-NL"), Text="Gegevens over uw inkomsten en vermogen worden opgehaald bij de belastingdienst",Id="2" },
            new TextEntity(){Culture = CultureInfo.GetCultureInfo("nl-NL"), Text="Gegevens over uw inkomsten en vermogen zijn opgehaald bij de belastingdienst",Id="3" },
            new TextEntity(){Culture = CultureInfo.GetCultureInfo("nl-NL"), Text="",Id="4" },
            new TextEntity(){Culture = CultureInfo.GetCultureInfo("nl-NL"), Text="",Id="5" },
            new TextEntity(){Culture = CultureInfo.GetCultureInfo("nl-NL"), Text="",Id="6" },
            new TextEntity(){Culture = CultureInfo.GetCultureInfo("nl-NL"), Text="",Id="7" },
            new TextEntity(){Culture = CultureInfo.GetCultureInfo("nl-NL"), Text="",Id="8" }
        };
    }
}
