﻿using System.Globalization;

namespace Inkomensloket.ComponentLibrary.Cms.Repositories
{
    public interface IMultilingualEntity
    {
        CultureInfo Culture { get; set; }
        string Id { get; set; }
    }
}
