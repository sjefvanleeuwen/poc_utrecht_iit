﻿using System;
using System.Globalization;

namespace Inkomensloket.ComponentLibrary.Cms.Repositories
{
    public class TextEntity : IMultilingualEntity
    {
        public CultureInfo Culture { get; set; }

        public string Text { get; set; }
        public string Id { get; set; }
    }
}
