﻿using Microsoft.AspNetCore.Components;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Inkomensloket.ComponentLibrary.Cms.Repositories
{
    public class GetDataScreenRepositoryInMemory : IMultilingualRepository<GetDataScreenEntity>
    {
        public IMultilingualRepository<TextEntity> repository { get; set; }

        public GetDataScreenEntity Get(string key, CultureInfo culture)
        {
            return EntityData().Single(p => p.Id == key && p.Culture == culture);
        }

        public GetDataScreenRepositoryInMemory(IMultilingualRepository<TextEntity> repo)
        {
            repository = repo;
        }

        private IEnumerable<GetDataScreenEntity> EntityData()
        {
            var data = new List<GetDataScreenEntity>();
            data.Add(new GetDataScreenEntity()
            {
                Id = "1",
                Culture = CultureInfo.GetCultureInfo("nl-NL"),
                Title = repository.Get("1", CultureInfo.GetCultureInfo("nl-NL")).Text,
                Loading = repository.Get("2", CultureInfo.GetCultureInfo("nl-NL")).Text,
                Loaded = repository.Get("3", CultureInfo.GetCultureInfo("nl-NL")).Text
            });
            return data;
        }
    }
}
