﻿using Microsoft.AspNetCore.Components;
using System.Globalization;

namespace Inkomensloket.ComponentLibrary.Cms.Repositories
{
    public class GetDataScreenEntity : IMultilingualEntity
    {
        public CultureInfo Culture { get; set; }
        public string Id { get; set; }

        public string Title { get; set; }
        public string Loading { get; set; }
        public string Loaded { get; set; }
    }
}
