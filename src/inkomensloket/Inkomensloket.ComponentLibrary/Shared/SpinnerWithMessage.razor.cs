﻿using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;

namespace Inkomensloket.ComponentLibrary
{
    public partial class SpinnerWithMessage
    {
        Spinner spinner { get; set; }

        [Parameter]
        public RenderFragment MessageLoading { get; set; }

        [Parameter]
        public RenderFragment MessageLoaded { get; set; }

        public void SetLoading(bool isLoading)
        {
            spinner.SetState(isLoading);
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await Task.Delay(5000);
                spinner.SetState(false);
                StateHasChanged();
            }
            base.OnAfterRenderAsync(firstRender);
        }
    }
}
