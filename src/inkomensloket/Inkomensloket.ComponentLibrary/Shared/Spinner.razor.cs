﻿using Microsoft.AspNetCore.Components;
namespace Inkomensloket.ComponentLibrary
{
    public partial class Spinner
    {
        [Parameter] public bool InProgress { get; set; }

        public void SetState(bool progress)
        {
            InProgress = progress;
            StateHasChanged();
        }
    }
}
