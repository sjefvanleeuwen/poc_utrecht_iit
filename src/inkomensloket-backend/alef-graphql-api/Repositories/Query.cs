using HotChocolate;

namespace alef_graphql_api
{
    public class Query
    {
        public Uitvoer__besluit__iit Request(Invoer__aanvraag__iit invoer,
        [Service] IIitRepository repository) =>
               repository.Request(invoer);
    }
}