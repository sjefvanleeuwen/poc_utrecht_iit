namespace alef_graphql_api
{
    public class IitRepository : IIitRepository
    {
        public Uitvoer__besluit__iit Request(Invoer__aanvraag__iit invoer)
        {
            return new Uitvoer__besluit__iit()
            {
                uitTeKerenToeslagBedrag = 1235,
                rechtBeschrijving = "stub static data",
                uitTeKerenToeslagBedragSpecified = true,

            };
        }
    }
}