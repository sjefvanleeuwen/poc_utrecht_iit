﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;
using StrawberryShake;
using StrawberryShake.Configuration;
using StrawberryShake.Http;
using StrawberryShake.Http.Subscriptions;
using StrawberryShake.Transport;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class IitResultParser
        : JsonResultParserBase<IIit>
    {
        private readonly IValueSerializer _stringSerializer;
        private readonly IValueSerializer _decimalSerializer;
        private readonly IValueSerializer _booleanSerializer;

        public IitResultParser(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _stringSerializer = serializerResolver.Get("String");
            _decimalSerializer = serializerResolver.Get("Decimal");
            _booleanSerializer = serializerResolver.Get("Boolean");
        }

        protected override IIit ParserData(JsonElement data)
        {
            return new Iit
            (
                ParseIitRequest(data, "request")
            );

        }

        private global::alef_graphql_client.IUitvoerBesluitIit ParseIitRequest(
            JsonElement parent,
            string field)
        {
            if (!parent.TryGetProperty(field, out JsonElement obj))
            {
                return null;
            }

            if (obj.ValueKind == JsonValueKind.Null)
            {
                return null;
            }

            return new UitvoerBesluitIit
            (
                DeserializeNullableString(obj, "rechtBeschrijving"),
                DeserializeDecimal(obj, "uitTeKerenToeslagBedrag"),
                DeserializeBoolean(obj, "uitTeKerenToeslagBedragSpecified")
            );
        }

        private string DeserializeNullableString(JsonElement obj, string fieldName)
        {
            if (!obj.TryGetProperty(fieldName, out JsonElement value))
            {
                return null;
            }

            if (value.ValueKind == JsonValueKind.Null)
            {
                return null;
            }

            return (string)_stringSerializer.Deserialize(value.GetString());
        }

        private decimal DeserializeDecimal(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (decimal)_decimalSerializer.Deserialize(value.GetDecimal());
        }

        private bool DeserializeBoolean(JsonElement obj, string fieldName)
        {
            JsonElement value = obj.GetProperty(fieldName);
            return (bool)_booleanSerializer.Deserialize(value.GetBoolean());
        }
    }
}
