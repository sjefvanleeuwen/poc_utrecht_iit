﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial interface IUitvoerBesluitIit
    {
        string RechtBeschrijving { get; }

        decimal UitTeKerenToeslagBedrag { get; }

        bool UitTeKerenToeslagBedragSpecified { get; }
    }
}
