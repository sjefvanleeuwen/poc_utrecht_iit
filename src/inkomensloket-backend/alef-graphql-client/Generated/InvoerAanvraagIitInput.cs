﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class Invoer__aanvraag__iitInput
    {
        public Optional<bool> Alleenstaand { get; set; }

        public Optional<bool> AowLeeftijdBehaald { get; set; }

        public Optional<decimal> InkomenPerMaand { get; set; }

        public Optional<bool> OuderDan21 { get; set; }

        public Optional<bool> ThuiswonendeKinderen { get; set; }

        public Optional<decimal> Vermogen { get; set; }

        public Optional<string> Woonplaats { get; set; }
    }
}
