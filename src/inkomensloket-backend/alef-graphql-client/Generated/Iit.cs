﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class Iit
        : IIit
    {
        public Iit(
            global::alef_graphql_client.IUitvoerBesluitIit request)
        {
            Request = request;
        }

        public global::alef_graphql_client.IUitvoerBesluitIit Request { get; }
    }
}
