﻿using System;
using System.Collections;
using System.Collections.Generic;
using StrawberryShake;

namespace alef_graphql_client
{
    [System.CodeDom.Compiler.GeneratedCode("StrawberryShake", "11.0.0")]
    public partial class InvoerAanvraagIitInputSerializer
        : IInputSerializer
    {
        private bool _needsInitialization = true;
        private IValueSerializer _booleanSerializer;
        private IValueSerializer _decimalSerializer;
        private IValueSerializer _stringSerializer;

        public string Name { get; } = "Invoer__aanvraag__iitInput";

        public ValueKind Kind { get; } = ValueKind.InputObject;

        public Type ClrType => typeof(Invoer__aanvraag__iitInput);

        public Type SerializationType => typeof(IReadOnlyDictionary<string, object>);

        public void Initialize(IValueSerializerCollection serializerResolver)
        {
            if (serializerResolver is null)
            {
                throw new ArgumentNullException(nameof(serializerResolver));
            }
            _booleanSerializer = serializerResolver.Get("Boolean");
            _decimalSerializer = serializerResolver.Get("Decimal");
            _stringSerializer = serializerResolver.Get("String");
            _needsInitialization = false;
        }

        public object Serialize(object value)
        {
            if (_needsInitialization)
            {
                throw new InvalidOperationException(
                    $"The serializer for type `{Name}` has not been initialized.");
            }

            if (value is null)
            {
                return null;
            }

            var input = (Invoer__aanvraag__iitInput)value;
            var map = new Dictionary<string, object>();

            if (input.Alleenstaand.HasValue)
            {
                map.Add("alleenstaand", SerializeNullableBoolean(input.Alleenstaand.Value));
            }

            if (input.AowLeeftijdBehaald.HasValue)
            {
                map.Add("aowLeeftijdBehaald", SerializeNullableBoolean(input.AowLeeftijdBehaald.Value));
            }

            if (input.InkomenPerMaand.HasValue)
            {
                map.Add("inkomenPerMaand", SerializeNullableDecimal(input.InkomenPerMaand.Value));
            }

            if (input.OuderDan21.HasValue)
            {
                map.Add("ouderDan21", SerializeNullableBoolean(input.OuderDan21.Value));
            }

            if (input.ThuiswonendeKinderen.HasValue)
            {
                map.Add("thuiswonendeKinderen", SerializeNullableBoolean(input.ThuiswonendeKinderen.Value));
            }

            if (input.Vermogen.HasValue)
            {
                map.Add("vermogen", SerializeNullableDecimal(input.Vermogen.Value));
            }

            if (input.Woonplaats.HasValue)
            {
                map.Add("woonplaats", SerializeNullableString(input.Woonplaats.Value));
            }

            return map;
        }

        private object SerializeNullableBoolean(object value)
        {
            return _booleanSerializer.Serialize(value);
        }
        private object SerializeNullableDecimal(object value)
        {
            return _decimalSerializer.Serialize(value);
        }
        private object SerializeNullableString(object value)
        {
            if (value is null)
            {
                return null;
            }


            return _stringSerializer.Serialize(value);
        }

        public object Deserialize(object value)
        {
            throw new NotSupportedException(
                "Deserializing input values is not supported.");
        }
    }
}
