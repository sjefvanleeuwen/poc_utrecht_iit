# Setup

Client project werd als volgt opgezet:

```
dotnet new
dotnet new tool-manifest
dotnet tool install StrawberryShake.Tools --local --version 11.0.0-preview.137
dotnet tool install StrawberryShake.Tools --version 11.0.0-preview.137
dotnet add package Microsoft.Extensions.Http --version 5.0.0
```

The Graph wordt vanaf de alef-graphql-api server gepulled

```
dotnet graphql init http://localhost:5080/graphql -n AlefGraphQlClient
```

Maak een query of mutation aan waarvoor je client code wilt genereren in ene nieuw bestand. In dit geval een query voor de IIT graph. Het bestand wordt bewaard als `IitQuery.graphql`

```
query iit($invoer:Invoer__aanvraag__iitInput) {
  request(invoer: $invoer){
    rechtBeschrijving
    uitTeKerenToeslagBedrag
    uitTeKerenToeslagBedragSpecified
  }
}
```

De client kan gebuild worden door:

```
dotnet build
```

Dit genereert een folder `./Generated` en deze bevat de client.

## Integratietests

De client werd opgezet als volgt:

```
mkdir alef-graph-gql-client-tests
cd alef-graph-gql-client-tests
dotnet new xunit
dotnet add reference ..\alef-graphql-client\
```

In de unit test doen we een simpele integratietest tegen de stub.
